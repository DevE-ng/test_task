--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Drop databases (except postgres and template1)
--

DROP DATABASE test;




--
-- Drop roles
--

DROP ROLE root;


--
-- Roles
--

CREATE ROLE root;
ALTER ROLE root WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS PASSWORD 'SCRAM-SHA-256$4096:gVflHuPnyMwsaKiqsuC3xQ==$3fWwoC9AW6QR/WHkjDFphw+sGpUjA8eSfGRhvWZY01A=:y9MzhwB1fSpB/Z0TbMN8cWOFZsmLXjK0p/FnvpkldIk=';






--
-- Databases
--

--
-- Database "template1" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2 (Debian 14.2-1.pgdg110+1)
-- Dumped by pg_dump version 14.2 (Debian 14.2-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

UPDATE pg_catalog.pg_database SET datistemplate = false WHERE datname = 'template1';
DROP DATABASE template1;
--
-- Name: template1; Type: DATABASE; Schema: -; Owner: root
--

CREATE DATABASE template1 WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';


ALTER DATABASE template1 OWNER TO root;

\connect template1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE template1; Type: COMMENT; Schema: -; Owner: root
--

COMMENT ON DATABASE template1 IS 'default template for new databases';


--
-- Name: template1; Type: DATABASE PROPERTIES; Schema: -; Owner: root
--

ALTER DATABASE template1 IS_TEMPLATE = true;


\connect template1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE template1; Type: ACL; Schema: -; Owner: root
--

REVOKE CONNECT,TEMPORARY ON DATABASE template1 FROM PUBLIC;
GRANT CONNECT ON DATABASE template1 TO PUBLIC;


--
-- PostgreSQL database dump complete
--

--
-- Database "postgres" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2 (Debian 14.2-1.pgdg110+1)
-- Dumped by pg_dump version 14.2 (Debian 14.2-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE postgres;
--
-- Name: postgres; Type: DATABASE; Schema: -; Owner: root
--

CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';


ALTER DATABASE postgres OWNER TO root;

\connect postgres

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE postgres; Type: COMMENT; Schema: -; Owner: root
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- PostgreSQL database dump complete
--

--
-- Database "test" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2 (Debian 14.2-1.pgdg110+1)
-- Dumped by pg_dump version 14.2 (Debian 14.2-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: test; Type: DATABASE; Schema: -; Owner: root
--

CREATE DATABASE test WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';


ALTER DATABASE test OWNER TO root;

\connect test

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: databasechangelog; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20),
    contexts character varying(255),
    labels character varying(255),
    deployment_id character varying(10)
);


ALTER TABLE public.databasechangelog OWNER TO root;

--
-- Name: databasechangeloglock; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);


ALTER TABLE public.databasechangeloglock OWNER TO root;

--
-- Name: sectors_sequence; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.sectors_sequence
    START WITH 69
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sectors_sequence OWNER TO root;

--
-- Name: sectors; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.sectors (
    id bigint DEFAULT nextval('public.sectors_sequence'::regclass) NOT NULL,
    name character varying(500) NOT NULL,
    parent_id bigint,
    active boolean DEFAULT false NOT NULL
);


ALTER TABLE public.sectors OWNER TO root;

--
-- Name: sessions_sequence; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.sessions_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sessions_sequence OWNER TO root;

--
-- Name: sessions; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.sessions (
    id bigint DEFAULT nextval('public.sessions_sequence'::regclass) NOT NULL,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL
);


ALTER TABLE public.sessions OWNER TO root;

--
-- Name: user_sectors; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.user_sectors (
    id bigint DEFAULT nextval('public.sectors_sequence'::regclass) NOT NULL,
    username character varying(500) NOT NULL,
    agreed_to_terms boolean NOT NULL,
    session_id bigint NOT NULL
);


ALTER TABLE public.user_sectors OWNER TO root;

--
-- Name: user_sectors_to_sectors; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.user_sectors_to_sectors (
    user_sector_id bigint NOT NULL,
    sector_id bigint NOT NULL
);


ALTER TABLE public.user_sectors_to_sectors OWNER TO root;

--
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) FROM stdin;
1	Ruslan Aysin	classpath:/db/changelog/db.changelog-master.yaml	2022-04-13 16:14:16.329168	1	EXECUTED	8:0bdfed056fd1e237d560b09b4f03fcf7	sql; createTable tableName=sectors; sql; createTable tableName=sessions; createTable tableName=user_sectors; createTable tableName=user_sectors_to_sectors; addUniqueConstraint constraintName=unq_user_sectors_id_session_id, tableName=user_sectors		\N	4.5.0	\N	\N	9855655758
\.


--
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.databasechangeloglock (id, locked, lockgranted, lockedby) FROM stdin;
1	f	\N	\N
\.


--
-- Data for Name: sectors; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.sectors (id, name, parent_id, active) FROM stdin;
1	Manufacturing	\N	t
2	Food and Beverage	1	t
3	Bakery & confectionery products	2	t
4	Sweets & snack food	2	t
5	Meat & meat products	2	t
6	Milk & dairy products	2	t
7	Fish & fish products	2	t
8	Other	2	t
9	Beverages	2	t
10	Printing	1	t
11	Book/Periodicals printing	10	t
12	Labelling and packaging printing	10	t
13	Advertising	10	t
14	Construction materials	1	t
15	Wood	1	t
16	Other (Wood)	15	t
17	Wooden houses	15	t
18	Wooden building materials	15	t
19	Metalworking	1	t
20	Construction of metal structures	19	t
21	Houses and buildings	19	t
22	Metal products	19	t
23	Metal works	19	t
24	Gas, Plasma, Laser cutting	23	t
25	CNC-machining	23	t
26	Forgings, Fasteners	23	t
27	MIG, TIG, Aluminum welding	23	t
28	Plastic and Rubber	1	t
29	Plastic goods	28	t
30	Plastic processing technology	28	t
31	Plastics welding and processing	30	t
32	Blowing	30	t
33	Moulding	30	t
34	Plastic profiles	28	t
35	Packaging	28	t
36	Service	1	t
37	Information Technology and Telecommunications	36	t
38	Data processing, Web portals, E-marketing	37	t
39	Telecommunications	37	t
40	Programming, Consultancy	37	t
41	Software, Hardware	37	t
42	Tourism	36	t
43	Engineering	36	t
44	Transport and Logistics	36	t
45	Rail	44	t
46	Road	44	t
47	Air	44	t
48	Water	44	t
49	Translation services	36	t
50	Business services	36	t
51	Furniture	1	t
52	Bathroom/sauna	51	t
53	Living room	51	t
54	Project furniture	51	t
55	Office	51	t
56	Bedroom	51	t
57	Children`s room	51	t
58	Kitchen	51	t
59	Other (Furniture)	51	t
60	Outdoor	51	t
61	Textile and Clothing	1	t
62	Textile	61	t
63	Clothing	61	t
64	Other	1	t
65	Energy technology	64	t
66	Creative industries	64	t
67	Environment	64	t
68	Electronics and Optics	1	t
\.


--
-- Data for Name: sessions; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.sessions (id, start_time, end_time) FROM stdin;
\.


--
-- Data for Name: user_sectors; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.user_sectors (id, username, agreed_to_terms, session_id) FROM stdin;
\.


--
-- Data for Name: user_sectors_to_sectors; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.user_sectors_to_sectors (user_sector_id, sector_id) FROM stdin;
\.


--
-- Name: sectors_sequence; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.sectors_sequence', 69, false);


--
-- Name: sessions_sequence; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.sessions_sequence', 1, false);


--
-- Name: databasechangeloglock databasechangeloglock_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.databasechangeloglock
    ADD CONSTRAINT databasechangeloglock_pkey PRIMARY KEY (id);


--
-- Name: sectors pk_sectors_id; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.sectors
    ADD CONSTRAINT pk_sectors_id PRIMARY KEY (id);


--
-- Name: sessions pk_sessions_id; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT pk_sessions_id PRIMARY KEY (id);


--
-- Name: user_sectors pk_user_sectors_id; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.user_sectors
    ADD CONSTRAINT pk_user_sectors_id PRIMARY KEY (id);


--
-- Name: user_sectors_to_sectors pk_usts_user_sector_id; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.user_sectors_to_sectors
    ADD CONSTRAINT pk_usts_user_sector_id PRIMARY KEY (user_sector_id, sector_id);


--
-- Name: user_sectors unq_user_sectors_id_session_id; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.user_sectors
    ADD CONSTRAINT unq_user_sectors_id_session_id UNIQUE (id, session_id);


--
-- Name: sectors fk_sectors_parent_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.sectors
    ADD CONSTRAINT fk_sectors_parent_id FOREIGN KEY (parent_id) REFERENCES public.sectors(id);


--
-- Name: user_sectors fk_user_sectors_sessions_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.user_sectors
    ADD CONSTRAINT fk_user_sectors_sessions_id FOREIGN KEY (session_id) REFERENCES public.sessions(id);


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--

